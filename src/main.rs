type VarName = String;

#[derive(Debug)]
enum Term {
    Var(VarName),
    App(Box<Term>, Box<Term>),
    Lam(VarName, Box<Term>),
}

use Term::*;

fn var(name: &str) -> Term {
    Var(String::from(name))
}

fn lam(name: &str, term: Term) -> Term {
    Lam(String::from(name), Box::new(term))
}

fn app(f: Term, x: Term) -> Term {
    App(Box::new(f), Box::new(x))
}

fn main() {
    let y = lam(
        "f",
        app(
            lam("x", app(var("f"), app(var("x"), var("x")))),
            lam("x", app(var("f"), app(var("x"), var("x")))),
        ),
    );
    println!("{:#?}", y);
}
